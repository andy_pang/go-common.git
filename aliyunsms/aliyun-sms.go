package aliyunsms

import (
    "fmt"

    "gitee.com/andy_pang/go-common/v2/config"
    openapi "github.com/alibabacloud-go/darabonba-openapi/client"
    dysmsapi "github.com/alibabacloud-go/dysmsapi-20170525/v2/client"
)

var client *dysmsapi.Client

// 使用AK&SK初始化账号Client
func CreateClient(accessKeyId string, accessKeySecret string) (*dysmsapi.Client, error) {
    if client != nil {
        return client, nil
    }
    config := &openapi.Config{}
    config.AccessKeyId = &accessKeyId
    config.AccessKeySecret = &accessKeySecret
    client = &dysmsapi.Client{}
    var err error
    client, err = dysmsapi.NewClient(config)
    return client, err
}

func SendSms(phoneNumbers, signName, templateCode, templateParam string) (err error) {
    client, err := CreateClient(config.Aliyun().AppId, config.Aliyun().Secret)
    if err != nil {
        return err
    }

    // 1.发送短信
    sendReq := &dysmsapi.SendSmsRequest{
        PhoneNumbers:  &phoneNumbers,
        SignName:      &signName,
        TemplateCode:  &templateCode,
        TemplateParam: &templateParam,
    }
    sendResp, err := client.SendSms(sendReq)
    if err != nil {
        return err
    }

    code := sendResp.Body.Code
    if *code != "OK" {
        return fmt.Errorf(*sendResp.Body.Message)
    }

    // bizId := sendResp.Body.BizId
    return nil
    // // 2. 等待 10 秒后查询结果
    // _err = util.Sleep(tea.Int(10000))
    // if _err != nil {
    //     return _err
    // }
    // // 3.查询结果
    // phoneNums := string_.Split(args[0], tea.String(","), tea.Int(-1))
    // for _, phoneNum := range phoneNums {
    //     queryReq := &dysmsapi.QuerySendDetailsRequest{
    //         PhoneNumber: util.AssertAsString(phoneNum),
    //         BizId:       bizId,
    //         SendDate:    time.Format(tea.String("yyyyMMdd")),
    //         PageSize:    tea.Int64(10),
    //         CurrentPage: tea.Int64(1),
    //     }
    //     queryResp, _err := client.QuerySendDetails(queryReq)
    //     if _err != nil {
    //         return _err
    //     }

    //     dtos := queryResp.Body.SmsSendDetailDTOs.SmsSendDetailDTO
    //     // 打印结果
    //     for _, dto := range dtos {
    //         if tea.BoolValue(util.EqualString(tea.String(tea.ToString(tea.Int64Value(dto.SendStatus))), tea.String("3"))) {
    //             console.Log(tea.String(tea.StringValue(dto.PhoneNum) + " 发送成功，接收时间: " + tea.StringValue(dto.ReceiveDate)))
    //         } else if tea.BoolValue(util.EqualString(tea.String(tea.ToString(tea.Int64Value(dto.SendStatus))), tea.String("2"))) {
    //             console.Log(tea.String(tea.StringValue(dto.PhoneNum) + " 发送失败"))
    //         } else {
    //             console.Log(tea.String(tea.StringValue(dto.PhoneNum) + " 正在发送中..."))
    //         }

    //     }
    // }
    // return _err
}

// func main() {
//     err := _main(tea.StringSlice(os.Args[1:]))
//     if err != nil {
//         panic(err)
//     }
// }
