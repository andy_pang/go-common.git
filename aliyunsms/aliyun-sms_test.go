package aliyunsms_test

import (
    "fmt"
    "testing"

    "gitee.com/andy_pang/go-common/v2/aliyunsms"
    "gitee.com/andy_pang/go-common/v2/config"
    "gitee.com/andy_pang/go-common/v2/wx"
)

func TestA(t *testing.T) {
    config.Init("/Users/andy/projects/order/beauty/merchant-server/build/config/config.json")
    err := aliyunsms.SendSms("17780583960", "成都百星果科技", "SMS_291556186", "{\"code\":\"1234\"}")
    if err != nil {
        fmt.Println(err)
    }
    err = aliyunsms.SendSms("17780583960", "成都百星果科技", "SMS_291556186", "{\"code\":\"5678\"}")
    if err != nil {
        fmt.Println(err)
    }
}

func TestB(t *testing.T) {

    appId := ""
    secret := ""
    params := map[string]any{
        "scene":      "merchantId=nAYXzeb1uI7oTwp-uQ8Riw%253D%253D",
        "page":       "pages/reservation/reservation-list/index",
        "check_path": false,
    }
    wx.GetUnlimitedQRCode(appId, secret, params)
}
