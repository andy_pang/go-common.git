package wx

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io"
    "net/http"
    "net/url"

    "gitee.com/andy_pang/go-common/v2/config"
)

func SendMessage(params any) error {

    return SendMessageBy(config.Wx().AppId, config.Wx().Secret, params)
}

//公众号网页发送消息
func SendMessageBy(appId, secret string, params any) error {
    token, err := GetAccessTokenBy(appId, secret)
    if err != nil {
        return err
    }
    address := fmt.Sprintf("%s/cgi-bin/message/template/send?access_token=%s", HOST, token)
    bytesData, err := json.Marshal(params)
    if err != nil {
        return err
    }
    res, err := http.Post(address, "application/json;charset=utf-8", bytes.NewBuffer([]byte(bytesData)))
    if err != nil {
        return err
    }
    defer res.Body.Close()
    content, err := io.ReadAll(res.Body)
    if err != nil {
        return err
    }
    result := make(map[string]any, 0)
    err = json.Unmarshal(content, &result)
    if err != nil {
        return err
    }
    if int(result["errcode"].(float64)) == 0 {
        return nil
    }
    return fmt.Errorf("Send Error:" + string(content))
}

//添加模板
func AddTemplate(appId, secret, shortId string, keywords []string) (string, error) {
    token, err := GetAccessTokenBy(appId, secret)
    if err != nil {
        return "", err
    }
    address := fmt.Sprintf("%s/cgi-bin/template/api_add_template?access_token=%s", HOST, token)
    bodyMap := map[string]any{
        "template_id_short": shortId,
        "keyword_name_list": keywords,
    }
    body, err := json.Marshal(bodyMap)
    if err != nil {
        return "", err
    }
    resp, err := http.Post(address, "application/json;charset=utf-8", bytes.NewBuffer([]byte(body)))
    if err != nil {
        return "", err
    }
    defer resp.Body.Close()
    body, err = io.ReadAll(resp.Body)
    if err != nil {
        return "", err
    }
    var response WxTemplateIdResponse
    err = json.Unmarshal(body, &response)
    if err != nil {
        return "", err
    }
    if response.ErrMsg != "ok" {
        msg := fmt.Sprintf("%v:%v", response.Errcode, response.ErrMsg)
        return "", fmt.Errorf(msg)
    }
    return response.TemplateId, nil
}

//获取模板列表
func GetTemplateList(appId, secret string) (WxTemplateListResponse, error) {
    token, err := GetAccessTokenBy(appId, secret)
    if err != nil {
        return WxTemplateListResponse{}, err
    }
    address := fmt.Sprintf("%s/cgi-bin/template/get_all_private_template?access_token=%s", HOST, token)
    urlPath, err := url.Parse(address)
    if err != nil {
        return WxTemplateListResponse{}, err
    }
    resp, err := http.Get(urlPath.String())
    if err != nil {
        return WxTemplateListResponse{}, err
    }
    defer resp.Body.Close()
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return WxTemplateListResponse{}, err
    }
    var templateList WxTemplateListResponse
    err = json.Unmarshal(body, &templateList)
    if err != nil {
        return WxTemplateListResponse{}, err
    }
    return templateList, nil
}

//小程序发送消息
func SendMessageMiniBy(appId, secret string, params any) error {
    token, err := GetAccessTokenBy(appId, secret)
    if err != nil {
        return err
    }
    address := fmt.Sprintf("%s/cgi-bin/message/subscribe/send?access_token=%s", HOST, token)
    bytesData, err := json.Marshal(params)
    if err != nil {
        return err
    }
    res, err := http.Post(address, "application/json;charset=utf-8", bytes.NewBuffer([]byte(bytesData)))
    if err != nil {
        return err
    }
    defer res.Body.Close()
    content, err := io.ReadAll(res.Body)
    if err != nil {
        return err
    }
    result := make(map[string]any, 0)
    err = json.Unmarshal(content, &result)
    if err != nil {
        return err
    }
    if int(result["errcode"].(float64)) == 0 {
        return nil
    }
    return fmt.Errorf("Send Error:" + string(content))
}
