package wx

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io"
    "net/http"

    "gitee.com/andy_pang/go-common/v2/config"
)

func GetPhoneNumber(code string) (string, error) {
    return GetPhoneNumberBy(config.Wx().AppId, config.Wx().Secret, code)
}
func GetPhoneNumberBy(appId, secret, code string) (string, error) {
    accessToken, err := GetAccessTokenBy(appId, secret)
    if len(accessToken) == 0 || err != nil {
        return "", err
    }
    address := fmt.Sprintf("%s/wxa/business/getuserphonenumber?access_token=%s", HOST, accessToken)
    bodyMap := map[string]string{
        "code": code,
    }
    body, err := json.Marshal(bodyMap)
    if err != nil {
        return "", err
    }
    resp, err := http.Post(address, "application/json;charset=utf-8", bytes.NewBuffer([]byte(body)))
    if err != nil {
        return "", err
    }
    defer resp.Body.Close()
    body, err = io.ReadAll(resp.Body)
    if err != nil {
        return "", err
    }
    var wxPhoneResponse WxPhoneResponse
    err = json.Unmarshal(body, &wxPhoneResponse)
    if err != nil {
        return "", err
    }
    return wxPhoneResponse.PhoneInfo.PhoneNumber, nil
}
