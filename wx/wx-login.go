package wx

import (
    "encoding/json"
    "fmt"
    "io"
    "net/http"
    "net/url"

    "gitee.com/andy_pang/go-common/v2/config"
)

//用于小程序登录
func WxLogin(code string) (WxLoginResponse, error) {
    return WxLoginBy(config.Wx().AppId, config.Wx().Secret, code)
}

//用于小程序登录
func WxLoginBy(appId, secret, code string) (WxLoginResponse, error) {
    urlPath, err := url.Parse(fmt.Sprintf("%s/sns/jscode2session", HOST))
    if err != nil {
        return WxLoginResponse{}, err
    }
    params := url.Values{}
    params.Set("appid", appId)
    params.Set("secret", secret)
    params.Set("js_code", code)
    params.Set("grant_type", "authorization_code")
    urlPath.RawQuery = params.Encode()

    resp, err := http.Get(urlPath.String())
    if err != nil {
        return WxLoginResponse{}, err
    }
    defer resp.Body.Close()
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return WxLoginResponse{}, err
    }
    var wxLoginResponse WxLoginResponse
    err = json.Unmarshal(body, &wxLoginResponse)
    if err != nil {
        return WxLoginResponse{}, err
    }
    return wxLoginResponse, nil
}

//用于公众号 网页登录
func WxOauth2LoginBy(appId, secret, code string) (WxLoginAccessToken, error) {
    urlPath, err := url.Parse(fmt.Sprintf("%s/sns/oauth2/access_token", HOST))
    if err != nil {
        return WxLoginAccessToken{}, err
    }
    params := url.Values{}
    params.Set("appid", appId)
    params.Set("secret", secret)
    params.Set("code", code)
    params.Set("grant_type", "authorization_code")
    urlPath.RawQuery = params.Encode()

    resp, err := http.Get(urlPath.String())
    if err != nil {
        return WxLoginAccessToken{}, err
    }
    defer resp.Body.Close()
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return WxLoginAccessToken{}, err
    }
    var loginInfo WxLoginAccessToken
    err = json.Unmarshal(body, &loginInfo)
    if err != nil {
        return WxLoginAccessToken{}, err
    }
    return loginInfo, nil
}
