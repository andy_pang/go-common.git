package wx

import "time"

const (
    HOST = "https://api.weixin.qq.com"
)

type WxLoginResponse struct {
    OpenId     string `json:"openid"`
    SessionKey string `json:"session_key"`
    Unionid    string `json:"unionid"`
    Errcode    int    `json:"errcode"`
    ErrMsg     string `json:"errmsg"`
}

type WxLoginAccessToken struct {
    AccessToken    string `json:"access_token"`
    ExpiresIn      int64  `json:"expires_in"`
    RefreshToken   string `json:"refresh_token"`
    OpenId         string `json:"openid"`
    Scope          string `json:"scope"`
    IsSnapshotuser string `json:"is_snapshotuser"`
    Unionid        string `json:"unionid"`
}

type WxTokenResponse struct {
    AccessToken string `json:"access_token"`
    ExpiresIn   int64  `json:"expires_in"`
    CreateAt    time.Time
}

type WxPhoneResponse struct {
    Errcode   int       `json:"errcode"`
    ErrMsg    string    `json:"errmsg"`
    PhoneInfo PhoneInfo `json:"phone_info"`
}

type PhoneInfo struct {
    PhoneNumber     string `json:"phoneNumber"`
    PurePhoneNumber string `json:"purePhoneNumber"`
    CountryCode     string `json:"countryCode"`
}

type WxTemplateIdResponse struct {
    Errcode    int    `json:"errcode"`
    ErrMsg     string `json:"errmsg"`
    TemplateId string `json:"template_id"`
}

type WxTemplateMsgResponse struct {
    Errcode int    `json:"errcode"`
    ErrMsg  string `json:"errmsg"`
    MsgId   int    `json:"msgid"`
}

type WxTemplateResponse struct {
    TemplateId      string `json:"template_id"`
    Title           string `json:"title"`
    PrimaryIndustry string `json:"primary_industry"`
    DeputyIndustry  string `json:"deputy_industry"`
    Content         string `json:"content"`
    Example         string `json:"example"`
}

type WxTemplateListResponse struct {
    TemplateList []WxTemplateResponse `json:"template_list"`
}

type WxErrResponse struct {
    Errcode int    `json:"errcode"`
    ErrMsg  string `json:"errmsg"`
}
