package wx

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io"
    "net/http"
)

//生成不受限制但参数长度收限制
func GetUnlimitedQRCode(appId, secret string, param any) ([]byte, error) {
    accessToken, err := GetAccessTokenBy(appId, secret)
    if len(accessToken) == 0 || err != nil {
        return nil, err
    }
    address := fmt.Sprintf("%s/wxa/getwxacodeunlimit?access_token=%s", HOST, accessToken)
    body, err := json.Marshal(param)
    if err != nil {
        return nil, err
    }
    resp, err := http.Post(address, "application/json;charset=utf-8", bytes.NewBuffer([]byte(body)))
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()
    body, err = io.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }
    var errResponse WxErrResponse
    err = json.Unmarshal(body, &errResponse)
    if err != nil {
        //不是json结构返回图片内容 为二进制数据
        return body, nil
    }
    return nil, fmt.Errorf(errResponse.ErrMsg)
}
