package wx

import (
    "encoding/json"
    "fmt"
    "io"
    "net/http"
    "net/url"
    "time"

    "gitee.com/andy_pang/go-common/v2/config"
)

var tokenMap map[string]WxTokenResponse

func GetAccessToken() (string, error) {
    return GetAccessTokenBy(config.Wx().AppId, config.Wx().Secret)
}

func GetAccessTokenBy(appId, secret string) (string, error) {
    if tokenMap == nil {
        tokenMap = make(map[string]WxTokenResponse)
    }
    var err error
    tokenInfo, ok := tokenMap[appId]
    if !ok || tokenInfo.CreateAt.IsZero() {
        tokenInfo, err = getAccessToken(appId, secret)
    } else if (time.Now().UnixMilli()-tokenInfo.CreateAt.UnixMilli())/1000 > (tokenInfo.ExpiresIn - 30) {
        tokenInfo, err = getAccessToken(appId, secret)
    }
    if err != nil {
        return "", err
    }
    tokenMap[appId] = tokenInfo
    return tokenInfo.AccessToken, nil
}

func getAccessToken(appId, secret string) (WxTokenResponse, error) {
    urlPath, err := url.Parse(fmt.Sprintf("%s/cgi-bin/token", HOST))
    if err != nil {
        return WxTokenResponse{}, err
    }
    params := url.Values{}
    params.Set("appid", appId)
    params.Set("secret", secret)
    params.Set("grant_type", "client_credential")
    urlPath.RawQuery = params.Encode()

    resp, err := http.Get(urlPath.String())
    if err != nil {
        return WxTokenResponse{}, err
    }
    defer resp.Body.Close()
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return WxTokenResponse{}, err
    }
    var wxTokenResponse WxTokenResponse
    err = json.Unmarshal(body, &wxTokenResponse)
    if err != nil {
        return WxTokenResponse{}, err
    }
    wxTokenResponse.CreateAt = time.Now()
    return wxTokenResponse, nil
}
