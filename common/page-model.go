package common

import "reflect"

var (
    DEFAULT_PAGE_SIZE = 20
    MAX_PAGE_SIZE     = 500
)

type PageQuery struct {
    Page     int `json:"page"`
    PageSize int `json:"pageSize"`
}

type PageResult struct {
    Data  any `json:"data"`
    Total int `json:"total"`
}

// query 里面必须包含 Page 和 PageSize 两个字段 并且都是 int类型 int8 int16 int64均可
func InitPageQuery(query any) {

    values := reflect.ValueOf(query).Elem()

    page := values.FieldByName("Page")
    pageSize := values.FieldByName("PageSize")

    if page.CanInt() && page.Int() <= 0 {
        page.SetInt(1)
    }
    if pageSize.CanInt() {
        if pageSize.Int() <= 0 {
            pageSize.SetInt(int64(DEFAULT_PAGE_SIZE))
        } else if pageSize.Int() > 500 {
            pageSize.SetInt(int64(MAX_PAGE_SIZE))
        }
    }

}

func NewPageResult(a any, total int) PageResult {
    return PageResult{
        Data:  a,
        Total: total,
    }
}
