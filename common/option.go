package common

type Option struct {
    Value    string `json:"value"`    //值
    Text     string `json:"text"`     //展示文本
    Disabled bool   `json:"disabled"` //是否可用
}
