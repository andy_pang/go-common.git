package config

import (
    "encoding/json"
    "fmt"
    "os"
    "path/filepath"

    "gitee.com/andy_pang/go-common/v2/utils/bos"
)

type ServerInfo struct {
    Port        int  `json:"port"`
    SkipInstall bool `json:"skipInstall"`
}

type DbInfo struct {
    UserName  string `json:"userName"`
    Password  string `json:"password"`
    Host      string `json:"host"`
    DbName    string `json:"dbName"`
    ConnParam string `json:"connParam"`
    PoolSize  int    `json:"poolSize"`
    DbType    string `json:"dbType"`
    DbPath    string `json:"dbPath"`
}

type CorsInfo struct {
    AllowOrigin      string `json:"allowOrigin"`
    AllowMethods     string `json:"allowMethods"`
    AllowHeaders     string `json:"allowHeaders"`
    ExposeHeaders    string `json:"exposeHeaders"`
    AllowCredentials string `json:"allowCredentials"`
}

type WxConfig struct {
    AppId  string `json:"appId"`
    Secret string `json:"secret"`
}

type AliyunConfig struct {
    AppId  string `json:"appId"`
    Secret string `json:"secret"`
    Host   string `json:"host"`
}

type ConfigInfo struct {
    ServerInfo ServerInfo   `json:"server"`
    DbInfo     DbInfo       `json:"db"`
    CorsInfo   CorsInfo     `json:"corsInfo"`
    Wx         WxConfig     `json:"wx"`
    Aliyun     AliyunConfig `json:"aliyun"`
    LoginInfo  LoginInfo    `json:"log"`
}

//日志配置
type LoginInfo struct {
    Level      string `json:"level"`      //日志级别
    FileName   string `json:"fileName"`   //日志文件名字
    LogDir     string `json:"logDir"`     // 日志存放目录
    DbLevel    string `json:"dbLevel"`    //数据库日志级别
    DbFileName string `json:"dbFileName"` //数据库日志文件
}

//配置文件json格式
// {
//     "server":{
//         "port":8080,
//         "skipInstall":false //是否跳过安装 true跳过
//     },
//     "db":{
//         "url":"账号:密码@(127.0.0.1:3306)/dbName?charset=utf8mb4&parseTime=true&loc=Asia%2FShanghai"
//         "userName":"连接数据库的用户名",
//         "password":"连接数据库的密码",
//         "host":"数据库地址 如127.0.0.1:3306",
//         "dbName":"数据库名字",
//         "connParam":"连接参数 默认 charset=utf8mb4&parseTime=true&loc=Asia%2FShanghai",
//         "dbType":"mysql 或 sqlite",
//         "dbPath":"文件数据的文件地址"
//     },
//     "corsInfo" :{
//         "allowOrigin":"配置为空则不开跨域配置 下面四项也将不配置",
//         "allowMethods":"*",
//         "allowHeaders":"*",
//         "exposeHeaders":"*",
//         "allowCredentials":"true",
//       },
//     "wx": {
//         "appId": "xxx",
//         "secret": "xxx"
//      },
//     "aliyun": {
//         "appId": "xxx",
//         "secret": "xxx",
//         "host": "xxx"
//      },
//     "log":{
//         "level":"INFO", //日志级别 Fatal Error Warn Info Debug Trace 参考 logrus.TraceLevel 等说明
//         "fileName":"server.log"
//         "logDir":"/var/logs"
//     }
// }

var configInfo ConfigInfo

func Init(defaultCfgFile string) {
    cfgFile := filepath.Join(bos.GetHomeDir(), "config", "config.json")
    _, err := os.Stat(cfgFile)
    if err != nil {
        cfgFile = defaultCfgFile
    }

    configByte, err := os.ReadFile(cfgFile)
    if err != nil {
        panic(fmt.Sprintf("load config error %v", err))
    }
    err = json.Unmarshal(configByte, &configInfo)
    if err != nil {
        panic(fmt.Sprintf("parse config error %v", err))
    }
}

func Server() ServerInfo {
    return configInfo.ServerInfo
}

func Db() DbInfo {
    return configInfo.DbInfo
}

func Cors() CorsInfo {
    return configInfo.CorsInfo
}

func SetDb(dbInfo DbInfo) {
    configInfo.DbInfo = dbInfo
}

func Wx() WxConfig {
    return configInfo.Wx
}

func Aliyun() AliyunConfig {
    return configInfo.Aliyun
}

func Log() LoginInfo {
    return configInfo.LoginInfo
}
