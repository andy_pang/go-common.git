package log

import (
    "fmt"
    "io"
    "path/filepath"
    "strings"
    "time"

    "gitee.com/andy_pang/go-common/v2/config"
    "gitee.com/andy_pang/go-common/v2/utils/bos"
    rotatelogs "github.com/lestrrat-go/file-rotatelogs"
    "github.com/sirupsen/logrus"
)

var Logger logrus.Logger

var LogWriter io.Writer

type MineFormatter struct{}

const TimeFormat = "2006-01-02 15:04:05"

func (s *MineFormatter) Format(entry *logrus.Entry) ([]byte, error) {

    msg := fmt.Sprintf("[%s] [%s] %s\n", time.Now().Local().Format(TimeFormat), strings.ToUpper(entry.Level.String()), entry.Message)

    return []byte(msg), nil
}

func Init() {
    path := getLogPath()
    /* 日志轮转相关函数
       `WithLinkName` 为最新的日志建立软连接
       `WithRotationTime` 设置日志分割的时间，隔多久分割一次
       WithMaxAge 和 WithRotationCount二者只能设置一个
         `WithMaxAge` 设置文件清理前的最长保存时间
         `WithRotationCount` 设置文件清理前最多保存的个数
    */
    initLogWriter(path)
    Logger.Formatter = &MineFormatter{}
    Logger.Level = getLogLevel()
    Logger.SetOutput(LogWriter)
}

func initLogWriter(path string) {
    if LogWriter != nil {
        return
    }
    // 下面配置日志每隔 1 天轮转一个新文件，保留最近 30天的日志文件，多余的自动清理掉。
    LogWriter, _ = rotatelogs.New(
        path+".%Y%m%d",
        rotatelogs.WithLinkName(path),
        rotatelogs.WithMaxAge(time.Duration(24*30)*time.Hour),
        rotatelogs.WithRotationTime(time.Duration(24)*time.Hour),
    )
}

func getLogPath() string {
    logFileName := strings.Trim(config.Log().FileName, " ")
    if len(logFileName) == 0 {
        logFileName = "server.log"
    }
    var path string
    if len(strings.Trim(config.Log().LogDir, " ")) == 0 {
        path = filepath.Join(bos.GetHomeDir(), "logs", logFileName)
    } else {
        path = filepath.Join(config.Log().LogDir, logFileName)
    }
    return path
}

func getLogLevel() logrus.Level {
    level := strings.ToUpper(strings.Trim(config.Log().Level, " "))
    if len(level) == 0 {
        return logrus.TraceLevel
    }
    if level == "FATAL" {
        return logrus.FatalLevel
    }
    if level == "ERROR" {
        return logrus.ErrorLevel
    }
    if level == "WARN" {
        return logrus.WarnLevel
    }
    if level == "INFO" {
        return logrus.InfoLevel
    }
    if level == "DEBUG" {
        return logrus.DebugLevel
    }
    if level == "TRACE" {
        return logrus.TraceLevel
    }
    return logrus.TraceLevel
}

func Trace(args ...interface{}) {
    Logger.Trace(args...)
}

func Debug(args ...interface{}) {
    Logger.Debug(args...)
}

func Info(args ...interface{}) {
    Logger.Info(args...)
}

func Print(args ...interface{}) {
    Logger.Print(args...)
}

func Warn(args ...interface{}) {
    Logger.Warn(args...)
}

func Warning(args ...interface{}) {
    Logger.Warning(args...)
}

func Error(args ...interface{}) {
    Logger.Error(args...)
}

func Fatal(args ...interface{}) {
    Logger.Fatal(args...)
}

func Panic(args ...interface{}) {
    Logger.Panic(args...)
}

func Traceln(args ...interface{}) {
    Logger.Traceln(args...)
}

func Debugln(args ...interface{}) {
    Logger.Debugln(args...)
}

func Infoln(args ...interface{}) {
    Logger.Infoln(args...)
}

func Println(args ...interface{}) {
    Logger.Println(args...)
}

func Warnln(args ...interface{}) {
    Logger.Warnln(args...)
}

func Warningln(args ...interface{}) {
    Logger.Warningln(args...)
}

func Errorln(args ...interface{}) {
    Logger.Errorln(args...)
}

func Fatalln(args ...interface{}) {
    Logger.Fatalln(args...)
}

func Panicln(args ...interface{}) {
    Logger.Panicln(args...)
}
