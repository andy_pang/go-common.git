//使用demo

package main_test

import (
    "embed"
    "net/http"
    "time"

    "gitee.com/andy_pang/go-common/v2/bgin"
    "gitee.com/andy_pang/go-common/v2/config"
    "gitee.com/andy_pang/go-common/v2/db"
    "gitee.com/andy_pang/go-common/v2/log"
    "github.com/gin-gonic/gin"
)

//go:embed html/*
var fs embed.FS

func main() {
    Init()

    bgin.LoggerHandler = bgin.DefaultLogHandler(log.LogWriter, 2*time.Second)
    //当没有 fs 是 此处传空
    bgin.StartHttp(&fs, initRoute)
}

func Init() {
    gin.SetMode(gin.DebugMode)
    defaultFile := "config.json"
    config.Init(defaultFile)
    log.Init()
    db.Init()

}

func initRoute(r *gin.Engine) {
    r.GET("/abc.json", func(c *gin.Context) {
        m := make(map[string]string)
        m["code"] = "200"
        c.JSON(http.StatusOK, m)
    })
}
