package util

import (
	"fmt"
	"strconv"
	"strings"
)

type ordered interface {
    ~int | ~int8 | ~int16 | ~int32 | ~int64 |
    ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr |
    ~float32 | ~float64 | ~string
  }

  type num interface {
    ~int | ~int8 | ~int16 | ~int32 | ~int64 |
    ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr |
    ~float32 | ~float64
  }

// 判断list中是否包含某值
func InAny[T any](c T, list []T, equal func(T,T) bool) bool {
    for _, s := range list {
        if equal(c,s) {
            return true
        }
    }
    return false
}

// 判断list中是否包含某值
func In[T comparable](c T, list []T) bool {
    set := make(map[T]struct{}, len(list))
    for _, v := range list {
        set[v] = struct{}{}
    }
    _,ok := set[c]
    return ok
}

func Filter[T any](list []T, match func(T) bool) []T {
    j :=0
    for _, e := range list {
        if match(e) {
            list[j] = e
            j++
        }
    }
    return list[0:j]
}

func Join[T any](elems []T, sep string) string {
    list := make([]string, len(elems))
    for i, v := range elems {
        list[i] = fmt.Sprintf("%v", v)
    }
    return strings.Join(list, sep)
}

func TotalValueMap[T num](m map[string]T) T {
    var total T
     for _, v := range m {
         total += v
     }
     return total
 }

 func MinValueMap[T ordered](m map[string]T) string {
     var min T
     first := true
     key := ""
     for k, v := range m {
         if first {
             key = k
             min = v
             first = false
         } else if v < min {
             key = k
             min = v
         }

     }
     return key
 }

 func MaxValueMap[T ordered](m map[string]T) string {
     var max T
     first := true
     key := ""
     for k, v := range m {
         if first {
             key = k
             max = v
             first = false
         } else if v > max {
             key = k
             max = v
         }

     }
     return key
 }

 func SplitNum[T num](s, sep string) ([]T, error) {
    elems := strings.Split(s, sep)
    list := make([]T, len(elems))
    for i, v := range elems {
        vInt, err := strconv.Atoi(v)
        if err != nil {
            return nil, err
        }
        list[i] =  T(vInt)
    }
    return list, nil
}
