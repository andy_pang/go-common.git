package bos

import (
    "fmt"
    "os"
    "path/filepath"
    "strconv"
)

var HomeDir string

func GetHomeDir() string {
    if len(HomeDir) > 0 {
        return HomeDir
    }
    ex, err := os.Executable()
    if err != nil {
        return ""
    }
    exPath := filepath.Dir(ex)
    HomeDir = filepath.Dir(exPath)
    return HomeDir
}

func WritePid() {
    dir := GetHomeDir()
    pid := fmt.Sprintf("%d", os.Getpid())
    path := filepath.Join(dir, ".pid")
    os.WriteFile(path, []byte(pid), 0666)
}

func Stop() {
    dir := GetHomeDir()
    path := filepath.Join(dir, ".pid")
    con, err := os.ReadFile(path)
    if err != nil {
        fmt.Println("stop filed", err)
    }
    pid, err := strconv.Atoi(string(con))
    if err != nil {
        fmt.Println("stop filed", err)
    }
    process, err := os.FindProcess(pid)
    if err != nil {
        fmt.Println("stop filed", err)
    }
    err = process.Kill()
    if err != nil {
        fmt.Println("stop filed", err)
    }
    fmt.Println("stop sucess")
}
