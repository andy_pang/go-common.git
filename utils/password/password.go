package password

import (
    "crypto/md5"
    "fmt"

    "math/rand"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#^&*().,1234567890"

func PasswordHash(userName, password string) string {
    data := []byte(fmt.Sprintf("%s%s%s", userName, password, userName))
    has := md5.Sum(data)
    md5str := fmt.Sprintf("%x", has)
    return md5str
}

//新建一个密码
func NewPasswod(n int) string {
    b := make([]byte, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}
