package str

import (
    "strings"
)

func RepeatJoin(c string, count int, sep string) string {
    arr := make([]string, count)
    for i := 0; i < count; i++ {
        arr[i] = c
    }

    return strings.Join(arr, sep)
}
