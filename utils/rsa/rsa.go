package rsa

import (
    "crypto"
    "crypto/rand"
    "crypto/rsa"
    "crypto/sha256"
    "crypto/x509"
    "encoding/base64"
    "fmt"
)

// 加载公钥 如果是pem文件采用 pem.LoadPublicKey 方法
func LoadPublicKey(publicKey string) (*rsa.PublicKey, error) {
    publicKeyByte, err := base64.StdEncoding.DecodeString(publicKey)
    if err != nil {
        return nil, err
    }
    pubKey, err := x509.ParsePKIXPublicKey(publicKeyByte)
    if err != nil {
        return nil, err
    }
    if key, ok := pubKey.(*rsa.PublicKey); ok {
        return key, nil
    }
    return nil, fmt.Errorf("public key is not a rsa key type")
}

//加载私钥 如果是pem文件采用 pem.LoadPrivateKey 方法
func LoadPrivateKey(privateKey string) (*rsa.PrivateKey, error) {
    privateKeyByte, err := base64.StdEncoding.DecodeString(privateKey)
    if err != nil {
        return nil, err
    }
    privateKeyRsa, err := x509.ParsePKCS8PrivateKey(privateKeyByte)
    if err != nil {
        return nil, err
    }
    if key, ok := privateKeyRsa.(*rsa.PrivateKey); ok {
        return key, nil
    }
    return nil, fmt.Errorf("private key is not a rsa key type")
}

//Rsa2私钥签名
func Rsa2PrivateSign(signContent string, privateKey *rsa.PrivateKey, hash crypto.Hash) (string, error) {
    shaNew := hash.New()
    shaNew.Write([]byte(signContent))
    hashed := shaNew.Sum(nil)

    signature, err := rsa.SignPKCS1v15(rand.Reader, privateKey, hash, hashed)
    if err != nil {
        return "", err
    }
    return base64.StdEncoding.EncodeToString(signature), nil
}

// RSA2公钥验证签名
func Rsa2PubCheckSign(signContent, sign string, publicKey *rsa.PublicKey, hash crypto.Hash) error {
    hashed := sha256.Sum256([]byte(signContent))

    sig, err := base64.StdEncoding.DecodeString(sign)
    if err != nil {
        return err
    }
    return rsa.VerifyPKCS1v15(publicKey, hash, hashed[:], sig)
}
