package aes

import (
    "bytes"
    "crypto/aes"
    "crypto/cipher"
    "encoding/base64"
    "errors"
)

//PKCS7 填充模式
func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
    padding := blockSize - len(ciphertext)%blockSize
    //Repeat()函数的功能是把切片[]byte{byte(padding)}复制padding个，然后合并成新的字节切片返回
    padtext := bytes.Repeat([]byte{byte(padding)}, padding)
    return append(ciphertext, padtext...)
}

//填充的反向操作，删除填充字符串
func PKCS7UnPadding(origData []byte) ([]byte, error) {
    //获取数据长度
    length := len(origData)
    if length == 0 {
        return nil, errors.New("加密字符串错误！")
    } else {
        //获取填充字符串长度
        unpadding := int(origData[length-1])
        //截取切片，删除填充字节，并且返回明文
        n := length - unpadding
        if n < 0 || n > len(origData) {
            return nil, errors.New("加密字符串错误！")
        }
        return origData[:n], nil
    }
}

//实现加密
func AesEcrypt(origData []byte, key []byte) ([]byte, error) {
    //创建加密算法实例
    block, err := aes.NewCipher(key)
    if err != nil {
        return nil, err
    }
    //获取块的大小
    blockSize := block.BlockSize()
    //对数据进行填充，让数据长度满足需求
    origData = PKCS7Padding(origData, blockSize)
    //采用AES加密方法中CBC加密模式
    blocMode := cipher.NewCBCEncrypter(block, key[:blockSize])
    crypted := make([]byte, len(origData))
    //执行加密
    blocMode.CryptBlocks(crypted, origData)
    return crypted, nil
}

//实现解密
func AesDeCrypt(cypted []byte, key []byte) ([]byte, error) {
    //创建加密算法实例
    block, err := aes.NewCipher(key)
    if err != nil {
        return nil, err
    }
    //获取块大小
    blockSize := block.BlockSize()
    //创建加密客户端实例
    blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
    origData := make([]byte, len(cypted))
    //这个函数也可以用来解密
    blockMode.CryptBlocks(origData, cypted)
    //去除填充字符串
    origData, err = PKCS7UnPadding(origData)
    if err != nil {
        return nil, err
    }
    return origData, err
}

//Aes 加密 结果转成 base64
func EnCode(str, key []byte) (string, error) {
    result, err := AesEcrypt(str, key)
    if err != nil {
        return "", err
    }
    return base64.URLEncoding.EncodeToString(result), err
}

//Aes 解密 先 base64 解码
func DeCode(code string, key []byte) ([]byte, error) {
    //解密base64字符串
    strByte, err := base64.URLEncoding.DecodeString(code)
    if err != nil {
        return nil, err
    }
    //执行AES解密
    return AesDeCrypt(strByte, key)
}
