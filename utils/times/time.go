package times

import (
    "fmt"
    "time"
)

const (
    TimeFormat = "2006-01-02 15:04:05"
)

type Time time.Time

func (t *Time) UnmarshalJSON(data []byte) (err error) {
    timeStr := string(data)
    if len(timeStr) <= 0 {
        return
    }
    newTime, err := time.ParseInLocation("\""+TimeFormat+"\"", string(data), time.Local)
    *t = Time(newTime)
    return
}

func (t Time) MarshalJSON() ([]byte, error) {
    if t.IsZero() {
        return []byte("\"\""), nil
    }
    tt := fmt.Sprintf("\"%s\"", time.Time(t).Format(TimeFormat))
    return []byte(tt), nil
}

func (t Time) String() string {
    return time.Time(t).Format(TimeFormat)
}

func (t Time) IsZero() bool {
    return time.Time(t).IsZero()
}

func (t Time) Time() time.Time {
    return time.Time(t)
}

func FormatTime(t time.Time) string {
    if t.IsZero() {
        return ""
    }
    return t.Format(TimeFormat)
}

func ParseTime(str string) (time.Time, error) {
    return time.ParseInLocation(TimeFormat, str, time.Local)
}

func ParseTimes(str string) (Time, error) {
    newTime, err := time.ParseInLocation(TimeFormat, str, time.Local)
    if err != nil {
        return Time(newTime), err
    } else {
        return Time{}, err
    }
}

//获取一天的开始和结束时间 开始时间为当天0点 结束时间是下一天0点 查询应该左闭右开区间
func GetBeginAndEndDateTime(t time.Time) (time.Time, time.Time) {
    loc, _ := time.LoadLocation("Local")
    date := t.Format("2006-01-02")
    startDate := date + " 00:00:00"
    startTime, _ := time.ParseInLocation("2006-01-02 15:04:05", startDate, loc)
    endTime := startTime.AddDate(0, 0, 1)
    return startTime, endTime
}
