package alipay

import (
    "crypto"
    "fmt"
    "testing"
    "time"

    "gitee.com/andy_pang/go-common/v2/utils/rsa"
    "gitee.com/andy_pang/go-common/v2/utils/times"
    "gitee.com/andy_pang/go-common/v2/utils/util"
    "github.com/goccy/go-json"
)

const (
    private   = ""
    public    = ""
    aliPublic = ""
    appId     = " "
)

func TestA(t *testing.T) {
    tokenInfo, err := AuthorizationCode(appId, private, "6d82c9d32f854240aa888cfc1e87JX65", aliPublic)
    if err != nil {
        fmt.Println(err)
    } else {
        fmt.Println(tokenInfo.AccessToken)
    }
}

func TestB(t *testing.T) {
    s := "{\"msg\":\"Invalid Arguments\",\"code\":\"40002\",\"sub_msg\":\"授权码code无效\",\"sub_code\":\"isv.code-invalid\"}"
    fmt.Println(s)
    sign := "ZAa5TxDTII7yYaGfkJqSo6gWII7n5jKj9RzxnHKDomLQPQ+Wf4paSvm5rDDgALsROew4aNoHp9JTg1dyXmDHN55j0DEz0Z25HcFR0JswjLCuwFNI3x0cuq8+0sN6BGsOVLaYmHsJThaDDifUqLI4zQt+Qe8ieEd3YhaZY1qcMDl8BN27/ZNEslhNj0kFz2k+CDM8jRbyczXm2GWOicqduq0g9N9acHi4UdxQOLeJ5D/PRcYZFja3TgDLb3xQWG8ETiL41s8cStkWRtLC72VqG9GNAa6QiX75IOar55LKKSCbgS8sLpvVEmWXuT/e30EkVtHz/CBKVexzyps3qDS6pw=="
    pubKey, _ := rsa.LoadPublicKey(aliPublic)
    err := rsa.Rsa2PubCheckSign(s, sign, pubKey, crypto.SHA256)
    if err != nil {
        fmt.Println(err)
    } else {
        fmt.Println("验证通过")
    }
}

func TestC(t *testing.T) {
    param := map[string]any{
        "out_trade_no": "123",
    }
    p, _ := json.Marshal(param)
    r, err := TradePay(appId, private, aliPublic, string(p))
    fmt.Println(err)
    fmt.Println(r)
}

func TestD(t *testing.T) {
    method := "alipay.system.oauth.token"
    s := "{\"error_response\":{\"msg\":\"Invalid Arguments\",\"code\":\"40002\",\"sub_msg\":\"授权码code无效\",\"sub_code\":\"isv.code-invalid\"},\"sign\":\"ZAa5TxDTII7yYaGfkJqSo6gWII7n5jKj9RzxnHKDomLQPQ+Wf4paSvm5rDDgALsROew4aNoHp9JTg1dyXmDHN55j0DEz0Z25HcFR0JswjLCuwFNI3x0cuq8+0sN6BGsOVLaYmHsJThaDDifUqLI4zQt+Qe8ieEd3YhaZY1qcMDl8BN27/ZNEslhNj0kFz2k+CDM8jRbyczXm2GWOicqduq0g9N9acHi4UdxQOLeJ5D/PRcYZFja3TgDLb3xQWG8ETiL41s8cStkWRtLC72VqG9GNAa6QiX75IOar55LKKSCbgS8sLpvVEmWXuT/e30EkVtHz/CBKVexzyps3qDS6pw==\"}"
    s = "{\"alipay_system_oauth_token_response\":{\"access_token\":\"authusrB6c13c1ec5ce542df827f7e132e878A65\",\"alipay_user_id\":\"20881022278122842523878670117165\",\"auth_start\":\"2024-08-12 22:52:08\",\"expires_in\":1296000,\"re_expires_in\":2592000,\"refresh_token\":\"authusrB6f0b37ad8fb240faa2532904a2515X65\",\"user_id\":\"2088202804407654\"},\"sign\":\"egPQgLmzhuOypPnJ/9eLjrEVCdTJ97mobLdfJgOKho3kDVXyuzIIjEHIZ0uI+Jhrl54miDWvdD8PS8/URs8FBghRELsukVhhtLkky+0i9TFyD4dvn1c9pDdoZQ7k8KuyMoq+/TPd2JiDEL3CcO8TwLwi6uIwnjm6PokJCIfjxZdTrF2ORIc/kgiUhBokLD3l++9ryZUsqvYj5Jwsh7ExPKH1Cf4TGuzqdSnEHnB+2Op8tCryvFPAg4/oM4tnp42XQEIAPKBKVryI2snzrjuCQLAkhJh2vE90uWBpHN5IJMOubt4AaYFoDmu0ASBMcPmOcwWQWNEdKyxm5tXbk7Ez2A==\"}"
    _, err := aliPayRsa2PubCheckSign(s, aliPublic, method)
    fmt.Println(err)
}

func TestBindRoyaltyRelation(t *testing.T) {
    recever := RoyaltyReceiver{
        Name:    "庞先海",
        Type:    "loginName",
        Account: "17780583960",
        Memo:    "庞先海",
    }
    source, err := BindRoyaltyRelation(appId, private, aliPublic, []RoyaltyReceiver{recever}, "2019032200000001")
    fmt.Println(source)
    fmt.Println(err)
}

func TestUnBindRoyaltyRelation(t *testing.T) {
    recever := RoyaltyReceiver{
        Name:    "庞先海",
        Type:    "loginName",
        Account: "17780583960",
        Memo:    "庞先海",
    }
    source, err := UnbindRoyaltyRelation(appId, private, aliPublic, []RoyaltyReceiver{recever}, "2019032200000001")
    fmt.Println(source)
    fmt.Println(err)
}

func Test1(t *testing.T) {
    s, e := times.GetBeginAndEndDateTime(time.Now())
    fmt.Println(s)
    fmt.Println(e)
}

func Test2(t *testing.T) {
    m := map[string]string{"a": "1", "b": "2", "c": "100", "d": "1"}
    s := util.MaxValueMap(m)
    fmt.Println(s)
}
