package alipay

import (
    "encoding/json"
    "fmt"
)

type AlipayTradePayResponse struct {
    SourceData   string `json:"sourceData"`
    Code         string `json:"code"`
    Msg          string `json:"msg"`
    TradeNo      string `json:"trade_no"`
    OutTradeNo   string `json:"out_trade_no"`
    BuyerLogonId string `json:"buyer_logon_id"`
    TotalAmount  string `json:"total_amount"`
    Remark       string `json:"remark"`
}

//当面付 https://opendocs.alipay.com/apis/api_1/alipay.trade.pay
func TradePay(appId, privateKey, aliPublic, bizContent string) (AlipayTradePayResponse, error) {
    method := "alipay.trade.pay"
    param := map[string]string{
        "biz_content": bizContent,
    }
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, param)
    if err != nil {
        return AlipayTradePayResponse{}, err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return AlipayTradePayResponse{SourceData: string(resB)}, fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res AlipayTradePayResponse
        json.Unmarshal([]byte(sourceData), &res)
        res.SourceData = string(resB)
        return res, nil
    }
}
