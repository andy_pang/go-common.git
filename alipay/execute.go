//请求支付宝接口

package alipay

import (
    "io"
    "net/http"
    "net/url"
    "time"

    "gitee.com/andy_pang/go-common/v2/utils/times"
)

const GATEWAY_ADDRESS = "https://openapi.alipay.com/gateway.do"

type ErrorMsg struct {
    Msg     string `json:"msg"`
    Code    string `json:"code"`
    SubMsg  string `json:"sub_msg"`
    SubCode string `json:"sub_code"`
}

//Get方式请求支付宝接口
func Get(gatewayAddress, appId, method, privateKey string, param map[string]string) ([]byte, error) {
    timestamp := times.FormatTime(time.Now())
    version := "1.0"
    sign, err := aliPayRsa2Sign(appId, method, timestamp, privateKey, version, param)
    if err != nil {
        return nil, err
    }
    urlPath, err := url.Parse(gatewayAddress)
    if err != nil {
        return nil, err
    }
    params := url.Values{}
    params.Set("timestamp", timestamp)
    params.Set("method", method)
    params.Set("app_id", appId)
    params.Set("sign_type", "RSA2")
    params.Set("version", version)
    params.Set("charset", "UTF-8")
    params.Set("sign", sign)
    for k, v := range param {
        params.Set(k, v)
    }
    urlPath.RawQuery = params.Encode()
    resp, err := http.Get(urlPath.String())
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }
    return body, nil
}
