package alipay

import (
    "crypto"
    "fmt"
    "sort"
    "strings"

    "gitee.com/andy_pang/go-common/v2/utils/rsa"
)

//支付宝签名工具
func aliPayRsa2Sign(appId, method, timestamp, privateKey, version string, param map[string]string) (string, error) {
    m := make(map[string]string, 0)
    key := make([]string, 0)
    m["app_id"] = appId
    key = append(key, "app_id")
    m["charset"] = "UTF-8"
    key = append(key, "charset")
    m["method"] = method
    key = append(key, "method")
    m["sign_type"] = "RSA2"
    key = append(key, "sign_type")
    m["timestamp"] = timestamp
    key = append(key, "timestamp")
    m["version"] = version
    key = append(key, "version")
    for k, v := range param {
        m[k] = v
        key = append(key, k)
    }
    sort.Strings(key)

    var ss []string
    for _, k := range key {
        ss = append(ss, fmt.Sprintf("%s=%s", k, m[k]))
    }
    s := strings.Join(ss, "&")
    priKey, err := rsa.LoadPrivateKey(privateKey)
    if err != nil {
        return "", err
    }
    return rsa.Rsa2PrivateSign(s, priKey, crypto.SHA256)
}

func aliPayRsa2PubCheckSign(response, publicKey, methodName string) (string, error) {
    signKey := "\"sign\""
    siginIndex := strings.Index(response, signKey)
    methodKey := strings.ReplaceAll(methodName, ".", "_") + "_response"
    methodIndex := strings.Index(response, methodKey)
    var sign string
    var signData string
    if siginIndex > methodIndex {
        sign = response[siginIndex+len(signKey)+2 : len(response)-2]
        signData = response[methodIndex+len(methodKey)+2 : siginIndex-1]
    } else {
        sign = response[siginIndex+len(signKey)+2 : methodIndex-1]
        signData = response[methodIndex+len(methodKey)+2 : len(response)-2]
    }
    pubKey, err := rsa.LoadPublicKey(publicKey)
    if err != nil {
        return signData, err
    }
    return signData, rsa.Rsa2PubCheckSign(signData, sign, pubKey, crypto.SHA256)
}
