//分账相关接口
//https://opendocs.alipay.com/open/009yj8?pathHash=0ddab051&ref=api

package alipay

import (
    "encoding/json"
    "fmt"
)

type RoyaltyReceiver struct {
    Name          string `json:"name"`
    Type          string `json:"type"`    //支付宝账号对应的支付宝唯一用户号: userId ,支付宝登录号: loginName,支付宝openId: openId
    Account       string `json:"account"` //分账接收方账号。
    Memo          string `json:"memo"`    //分账关系描述
    LoginName     string `json:"login_name"`
    BindLoginName string `json:"bind_login_name"`
}

type RoyaltyBindResponse struct {
    Code string `json:"code"`
    Msg  string `json:"msg"`
}

//批量查询结果
type RoyaltyBatchResponse struct {
    SourceData      string            `json:"sourceData"`
    Code            string            `json:"code"`
    Msg             string            `json:"msg"`
    ResultCode      string            `json:"result_code"`
    ReceiverList    []RoyaltyReceiver `json:"receiver_list"`
    TotalPageNum    int               `json:"total_page_num"`
    TotalRecordNum  int               `json:"total_record_num"`
    CurrentPageNum  int               `json:"current_page_num"`
    CurrentPageSize int               `json:"current_page_size"`
}

//绑定分账账号
//返回原始字符串，错误(无错误则操作成功)
func BindRoyaltyRelation(appId, privateKey, aliPublic string, receverList []RoyaltyReceiver, outNo string) (string, error) {
    model := map[string]any{
        "receiver_list":  receverList,
        "out_request_no": outNo,
    }
    bizContentB, _ := json.Marshal(model)
    bizContent := string(bizContentB)
    method := "alipay.trade.royalty.relation.bind"
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, map[string]string{"biz_content": bizContent})
    if err != nil {
        return string(resB), err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return string(resB), fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res RoyaltyBindResponse
        json.Unmarshal([]byte(sourceData), &res)
        if res.Code != "10000" {
            return string(resB), fmt.Errorf(res.Msg)
        }
        return string(resB), nil
    }
}

//解绑分账账号
//返回原始字符串，错误(无错误则操作成功)
func UnbindRoyaltyRelation(appId, privateKey, aliPublic string, receverList []RoyaltyReceiver, outNo string) (string, error) {
    model := map[string]any{
        "receiver_list":  receverList,
        "out_request_no": outNo,
    }
    bizContentB, _ := json.Marshal(model)
    bizContent := string(bizContentB)
    method := "alipay.trade.royalty.relation.unbind"
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, map[string]string{"biz_content": bizContent})
    if err != nil {
        return string(resB), err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return string(resB), fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res RoyaltyBindResponse
        json.Unmarshal([]byte(sourceData), &res)
        if res.Code != "10000" {
            return string(resB), fmt.Errorf(res.Msg)
        }
        return string(resB), nil
    }
}

//分账账号关系查询
//返回原始字符串，错误(无错误则操作成功)
func RoyaltyRelationQuery(appId, privateKey, aliPublic string, outNo string, pageNum int, pageSize int) (RoyaltyBatchResponse, error) {
    model := map[string]any{
        "page_size":      pageSize,
        "out_request_no": outNo,
        "page_num":       pageNum,
    }
    bizContentB, _ := json.Marshal(model)
    bizContent := string(bizContentB)
    method := "alipay.trade.royalty.relation.batchquery"
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, map[string]string{"biz_content": bizContent})
    if err != nil {
        return RoyaltyBatchResponse{SourceData: string(resB)}, err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return RoyaltyBatchResponse{SourceData: string(resB)}, fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res RoyaltyBatchResponse
        json.Unmarshal([]byte(sourceData), &res)
        return res, nil
    }
}

//分账结算参数
type RoyaltySettleRequest struct {
    OutRequestNo      string                `json:"out_request_no"` //结算请求流水号，由商家自定义
    TradeNo           string                `json:"trade_no"`
    RoyaltyParameters []RoyaltyParameter    `json:"royalty_parameters"`
    OperatorId        string                `json:"operator_id"`
    RoyaltyMode       string                `json:"royalty_mode"`
    ExtendParams      SettleExtendParameter `json:"extend_params"`
}

type RoyaltyParameter struct {
    TransIn      string `json:"trans_in"`     //收入方账户 userId 或 cardAliasNo 或 loginName
    RoyaltyType  string `json:"royalty_type"` //分账: transfer, 营销补差: replenish
    TransOut     string `json:"trans_out"`
    TransOutType string `json:"trans_out_type"`
    TransInType  string `json:"trans_in_type"`
    Amount       string `json:"amount"` //分账的金额，单位为元
    Desc         string `json:"desc"`
    RoyaltyScene string `json:"royalty_scene"`
    TransInName  string `json:"trans_in_name"` //分账收款方姓名，上送则进行姓名与支付宝账号的一致性校验，校验不一致则分账失败。不上送则不进行姓名校验
}

type SettleExtendParameter struct {
    RoyaltyFinish string `json:"royalty_finish"`
}

//分账结算参数
type RoyaltySettleResponse struct {
    SourceData string `json:"sourceData"`
    Code       string `json:"code"`
    Msg        string `json:"msg"`
    TradeNo    string `json:"trade_no"`
    SettleNo   string `json:"settle_no"`
}

//分账结算 https://opendocs.alipay.com/open/c3b24498_alipay.trade.order.settle
func RoyaltySettle(appId, privateKey, aliPublic string, request RoyaltySettleRequest) (RoyaltySettleResponse, error) {
    bizContentB, _ := json.Marshal(request)
    bizContent := string(bizContentB)
    method := "alipay.trade.order.settle"
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, map[string]string{"biz_content": bizContent})
    if err != nil {
        return RoyaltySettleResponse{SourceData: string(resB)}, err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return RoyaltySettleResponse{SourceData: string(resB)}, fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res RoyaltySettleResponse
        json.Unmarshal([]byte(sourceData), &res)
        res.SourceData = string(resB)
        if res.Code != "10000" {
            return res, fmt.Errorf(res.Msg)
        }
        return res, nil
    }
}

//分账查询参数
type RoyaltyQueryRequest struct {
    SettleNo     string `json:"settle_no"`      //alipay.trade.order.settle 接口返回
    OutRequestNo string `json:"out_request_no"` //调用分账接口时指定的外部请求号。分账查询时需要和支付宝交易号一起传入
    TradeNo      string `json:"trade_no"`
}

//分账查询返回结果
type RoyaltyQueryResponse struct {
    SourceData        string              `json:"sourceData"`
    Code              string              `json:"code"`
    Msg               string              `json:"msg"`
    OutRequestNo      string              `json:"out_request_no"`
    OperationDt       string              `json:"operation_dt"` //分账受理时间
    RoyaltyDetailList []RoyaltyDetailInfo `json:"royalty_detail_list"`
}

type RoyaltyDetailInfo struct {
    OperationType string `json:"operation_type"`
    Amount        string `json:"amount"`     //分账的金额，单位为元
    State         string `json:"state"`      //分账状态，SUCCESS成功，FAIL失败，PROCESSING处理中
    ExecuteDt     string `json:"execute_dt"` //分账执行时间
    TransOut      string `json:"trans_out"`  //分账转出账号
    TransOutType  string `json:"trans_out_type"`
    TransIn       string `json:"trans_in"`
    TransInType   string `json:"trans_in_type"`
    DetailId      string `json:"detail_id"` //分账明细单号
    ErrorCode     string `json:"error_code"`
    ErrorEesc     string `json:"error_desc"`
}

//https://opendocs.alipay.com/open/9ef980b7_alipay.trade.order.settle.query
func RoyaltyQuery(appId, privateKey, aliPublic string, request RoyaltyQueryRequest) (RoyaltyQueryResponse, error) {
    bizContentB, _ := json.Marshal(request)
    bizContent := string(bizContentB)
    method := "alipay.trade.order.settle.query"
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, map[string]string{"biz_content": bizContent})
    if err != nil {
        return RoyaltyQueryResponse{SourceData: string(resB)}, err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return RoyaltyQueryResponse{SourceData: string(resB)}, fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res RoyaltyQueryResponse
        json.Unmarshal([]byte(sourceData), &res)
        res.SourceData = string(resB)
        return res, nil
    }
}
