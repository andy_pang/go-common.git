// 支付宝账号认证接口
// https://opendocs.alipay.com/open/218_1/105329?pathHash=436b8714

package alipay

import (
    "encoding/json"
    "fmt"
)

type AliPayTokenInfo struct {
    SourceData   string `json:"sourceData"`
    AccessToken  string `json:"access_token"`
    AlipayUserId string `json:"alipay_user_id"`
    AuthStart    string `json:"auth_start"`
    ExpiresIn    int    `json:"expires_in"`
    ReExpiresIn  int    `json:"re_expires_in"`
    RefreshToken string `json:"refresh_token"`
    UserId       string `json:"user_id"`
}

type AliPayUserInfo struct {
    SourceData string `json:"sourceData"`
    UserId     string `json:"user_id"`
    Avatar     string `json:"avatar"`
    City       string `json:"city"`
    NickName   string `json:"nick_name"`
    Province   string `json:"province"`
    Gender     string `json:"gender"`
    Mobile     string `json:"mobile"`
}

//获取支付认证token
func AuthorizationCode(appId, privateKey, authCode, aliPublic string) (AliPayTokenInfo, error) {
    method := "alipay.system.oauth.token"
    param := map[string]string{
        "code":       authCode,
        "grant_type": "authorization_code",
    }
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, param)
    if err != nil {
        return AliPayTokenInfo{}, err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return AliPayTokenInfo{SourceData: string(resB)}, fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res AliPayTokenInfo
        json.Unmarshal([]byte(sourceData), &res)
        return res, nil
    }

}

func GetUserInfo(appId, privateKey, aliPublic string, tokenInfo AliPayTokenInfo) (AliPayUserInfo, error) {
    method := "alipay.user.info.share"
    param := map[string]string{
        "auth_token": tokenInfo.AccessToken,
    }
    resB, err := Get(GATEWAY_ADDRESS, appId, method, privateKey, param)
    if err != nil {
        return AliPayUserInfo{}, err
    }
    sourceData, err := aliPayRsa2PubCheckSign(string(resB), aliPublic, method)
    if err != nil {
        return AliPayUserInfo{SourceData: string(resB)}, fmt.Errorf(string(resB) + ",sign check failed")
    } else {
        var res AliPayUserInfo
        json.Unmarshal([]byte(sourceData), &res)
        return res, nil
    }
}
