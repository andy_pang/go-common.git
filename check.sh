#!/bin/bash

DIR=$(cd $(dirname $0); pwd)

go_file_path=$(find $DIR -name "*.go")

for file_path in $go_file_path 
do   
    go vet $file_path
done   

