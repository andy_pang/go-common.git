package install

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "os"
    "path/filepath"
    "strings"

    "gitee.com/andy_pang/go-common/v2/bgin"
    "gitee.com/andy_pang/go-common/v2/config"
    "gitee.com/andy_pang/go-common/v2/db"
    "gitee.com/andy_pang/go-common/v2/log"
    "gitee.com/andy_pang/go-common/v2/utils/bos"
    "github.com/gin-gonic/gin"
)

type InstallRequest struct {
    config.DbInfo
    SkipScript bool `json:"skipScript"` //跳过脚本初始化
}

var MysqlScript string

var SqliteScript string

func InitController(r *gin.RouterGroup, url string) {
    r.POST(url, Install)
}

func InitIsSController(r *gin.RouterGroup, url string) {
    r.GET(url, func(c *gin.Context) {
        suc := IsInstall()
        bgin.GinSuccessResult(c, suc)
    })
}

//保存安装配置文件
func Install(c *gin.Context) {
    if IsInstall() {
        bgin.GinFailResult(c, http.StatusBadRequest, "无需重复安装")
        return
    }
    var request InstallRequest
    if err := bgin.ShouldBind(c, &request); err != nil {
        return
    }
    //先连接一下数据库 判断配置是否正确 配置不正确保存配置
    if _, err := db.ConnectDb(request.DbInfo); err != nil {
        log.Debug(err)
        bgin.GinFailResult(c, http.StatusInternalServerError, "配置错误"+err.Error())
        return
    }

    content, err := json.Marshal(request)
    if err != nil {
        log.Debug(err)
        bgin.GinFailResult(c, http.StatusInternalServerError, "保存配置文件失败")
        return
    }
    if err := ioutil.WriteFile(getConfigPath(), content, 0666); err != nil {
        log.Errorln(err)
        bgin.GinFailResult(c, http.StatusInternalServerError, "保存配置文件失败")
        return
    }

    //初始化配置
    if err := InitConfig(); err != nil {
        log.Errorln(err)
        bgin.GinFailResult(c, http.StatusInternalServerError, "保存配置文件失败")
        return
    }
    //数据库初始化
    if err := db.Init(); err != nil {
        log.Errorln(err)
        bgin.GinFailResult(c, http.StatusInternalServerError, "数据库配置错误")
        return
    }

    //初始化脚本
    if !request.SkipScript {
        if err := initScript(); err != nil {
            log.Errorln(err)
            bgin.GinFailResult(c, http.StatusInternalServerError, err.Error())
            return
        }
    }
    bgin.GinSuccessResult(c, true)
}

func InitConfig() error {
    if config.Server().SkipInstall {
        return nil
    }
    if dbInfo, err := loadConfig(); err == nil {
        config.SetDb(dbInfo)
    } else {
        return err
    }
    return nil
}

func IsInstall() bool {
    if config.Server().SkipInstall {
        return true
    }
    dbInfo, err := loadConfig()
    if err != nil || len(dbInfo.Host) <= 0 {
        return false
    } else {
        return true
    }
}

func initScript() error {
    var sqlList []string
    if db.DB_TYPE_MYSQL == config.Db().DbType {
        sqlList = strings.Split(MysqlScript, ";")
    } else if db.DB_TYPE_SQLITE == config.Db().DbType {
        sqlList = strings.Split(SqliteScript, ";")
    } else {
        return fmt.Errorf("数据类型不支持")
    }
    tx := db.Db().Begin()
    for _, sql := range sqlList {
        sql = strings.Trim(sql, "\n")
        sql = strings.Trim(sql, "\r")
        sql = strings.Trim(sql, " ")
        if len(sql) == 0 {
            continue
        }
        if err := tx.Exec(sql).Error; err != nil {
            if err.Error() == "Error 1065 (42000): Query was empty" {
                continue
            }
            tx.Rollback()
            return err
        }
    }
    tx.Commit()
    return nil
}

func loadConfig() (config.DbInfo, error) {
    path := getConfigPath()
    configByte, err := os.ReadFile(path)
    if err != nil {
        return config.DbInfo{}, err
    }
    dbInfo := config.DbInfo{}
    err = json.Unmarshal(configByte, &dbInfo)
    if err != nil {
        return config.DbInfo{}, err
    }
    return dbInfo, nil
}

func getConfigPath() string {
    return filepath.Join(bos.GetHomeDir(), "config", "install.json")
}
