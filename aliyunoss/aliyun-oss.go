package aliyunoss

import (
    "crypto/hmac"
    "crypto/sha1"
    "encoding/base64"
    "encoding/json"
    "time"

    "gitee.com/andy_pang/go-common/v2/bgin"
    "gitee.com/andy_pang/go-common/v2/config"
    "github.com/gin-gonic/gin"
)

func InitUploadInfo(r *gin.RouterGroup, url string, minute, m int) {
    r.GET(url, func(c *gin.Context) {
        policy := Policy(minute, m)
        signature := Signature(policy, config.Aliyun().Secret)
        res := map[string]string{
            "policy":      policy,
            "signature":   signature,
            "accessKeyId": config.Aliyun().AppId,
            "host":        config.Aliyun().Host,
        }
        bgin.GinSuccessResult(c, res)
    })
}

// minute 多少分钟内有效 m最大上传文件大小 单位M
func Policy(minute, m int) string {
    now := time.Now().Add(time.Duration(minute) * time.Minute)
    policyMap := map[string]any{
        "expiration": now.Format("2006-01-02T15:04:05.999Z"),
        "conditions": []any{[]any{"content-length-range", 0, m * 1024 * 1024}},
    }
    b, _ := json.Marshal(policyMap)
    return base64.StdEncoding.EncodeToString(b)
}

func Signature(policy, secret string) string {
    key := []byte(secret)
    mac := hmac.New(sha1.New, key)
    mac.Write([]byte(policy))

    //进行base64编码
    return base64.StdEncoding.EncodeToString(mac.Sum(nil))
}
