package id

import (
    "encoding/binary"
    "fmt"

    "gitee.com/andy_pang/go-common/v2/utils/aes"
)

var snow Snowflake = NewSnowflake(0)

func NextSnowId() int64 {
    return snow.NextId()
}

func EncodeId(id uint64, key string) string {
    if id == 0 || len(key) == 0 {
        return ""
    }
    b := make([]byte, 8)
    binary.BigEndian.PutUint64(b, id)
    ids, err := aes.EnCode(b, []byte(key))
    if err != nil {
        return fmt.Sprintf("%v", id)
    }
    return ids
}

func DecodeId(str string, key string) uint64 {
    if len(str) == 0 {
        return 0
    }
    b, err := aes.DeCode(str, []byte(key))
    if err != nil {
        return 0
    }
    return binary.BigEndian.Uint64(b)
}
