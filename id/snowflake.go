package id

import (
    "encoding/binary"
    "net"
    "sync"
    "time"
)

type Snowflake struct {
    lock      sync.Mutex
    timestamp int64
    workerId  int64
    sequence  int64
}

const (
    timestampBits = uint(41)
    workerIdBits  = uint(10)
    sequenceBits  = uint(12)

    // 需要注意的是由于 -1 在二进制上表示是：
    // 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
    // 所以想要求得 41bits 的 timestamp 最大值可以将 -1 向左位移 41 位，得到：
    // 11111111 11111111 11111110 00000000 00000000 00000000 00000000 00000000
    // 那么再和 -1 进行 ^异或运算：
    // 00000000 00000000 00000001 11111111 11111111 11111111 11111111 11111111
    timestampMax = -1 ^ (-1 << timestampBits)
    workerIdMax  = -1 ^ (-1 << workerIdBits)
    sequenceMax  = -1 ^ (-1 << sequenceBits)

    workerIdShift  = sequenceBits                // 机器id左移位数
    timestampShift = sequenceBits + workerIdBits // 时间戳左移位数
)

func NewSnowflake(wid int64) Snowflake {
    if wid == 0 {
        wid = int64(getMacAddrs())
    }
    return Snowflake{
        workerId: wid & workerIdMax,
    }
}

func (s *Snowflake) NextId() int64 {
    s.lock.Lock()
    now := time.Now().UnixNano() / 1000000 // 转毫秒
    for now < s.timestamp {
        //润表的情况
        now = time.Now().UnixNano() / 1000000
    }
    var id int64
    if now == s.timestamp {
        s.sequence = (s.sequence + 1) & sequenceMax
        if s.sequence == 0 {
            //sequence自增完成等待下一秒
            for now <= s.timestamp {
                now = time.Now().UnixNano() / 1000000
            }
        }
    } else {
        s.sequence = 0
    }
    s.timestamp = now
    id = s.timestamp<<timestampShift | s.workerId<<workerIdShift | s.sequence
    s.lock.Unlock()
    return id
}

func getMacAddrs() int32 {
    netInterfaces, err := net.Interfaces()
    if err != nil {
        return 0
    }
    for _, netInterface := range netInterfaces {
        if len(netInterface.HardwareAddr) == 0 {
            continue
        }
        macAddr := binary.BigEndian.Uint32(netInterface.HardwareAddr)
        return int32(macAddr)
    }
    return 0
}
