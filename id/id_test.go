package id_test

import (
    "fmt"
    "testing"

    "gitee.com/andy_pang/go-common/v2/common"
    "gitee.com/andy_pang/go-common/v2/id"
)

type Person struct {
    common.PageQuery
    Name string `json:"name"`
    Age  int    `json:"age"`
}

func TestID(t *testing.T) {
    ids := id.EncodeId(120, "n3pTPesz1UcPZUTM")
    fmt.Println(ids)

    i := id.DecodeId(ids, "n3pTPesz1UcPZUTM")
    fmt.Println(i)

    i = id.DecodeId(ids, "n3pTPesz1UcPZUTN")
    fmt.Println(i)
}
