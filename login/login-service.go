package login

import (
    "encoding/json"
    "fmt"
    "net/http"
    "strconv"
    "strings"
    "time"

    "gitee.com/andy_pang/go-common/v2/bgin"
    "gitee.com/andy_pang/go-common/v2/log"
    "gitee.com/andy_pang/go-common/v2/utils/aes"
    "gitee.com/andy_pang/go-common/v2/utils/str"
    "github.com/gin-gonic/gin"
)

type TokenInfo struct {
    UserId    string `json:"userId"`
    UserName  string `json:"userName"`  //用户名
    Name      string `json:"name"`      //姓名
    From      string `json:"from"`      //用户来源
    Role      string `json:"role"`      //用户角色
    Extra     any    `json:"extra"`     //额外信息
    Timestamp int64  `json:"timestamp"` //过期时间 毫秒
}

type LoginRes struct {
    UserName string `json:"userName"` //用户名
    Name     string `json:"name"`     //姓名
    Token    string `json:"token"`    //令牌
    Role     string `json:"role"`     //用户角色
}

var getTokenInfo func(*gin.Context) (TokenInfo, error)

var tokenKey string

var TokenExpire int

func InitLoginController(r *gin.RouterGroup, loginUrl string, getTokenBack func(*gin.Context) (TokenInfo, error), key string) {
    getTokenInfo = getTokenBack
    tokenKey = key
    r.POST(loginUrl, Login)
}

func InitRefreshTokenController(r *gin.RouterGroup, url string, expire int) {
    TokenExpire = expire
    r.POST(url, RefreshToken)
}

func InitIsLoginController(r *gin.RouterGroup, url string) {
    r.GET(url, IsLogin)
}

func Login(c *gin.Context) {
    tokenInfo, err := getTokenInfo(c)
    if err != nil {
        log.Errorln("login failed", err)
        bgin.GinFailResult(c, http.StatusBadRequest, "Login Failed")
        return
    }
    token, err := CreateToken(tokenInfo)
    if err != nil {
        log.Errorln("login failed", err)
        bgin.GinFailResult(c, http.StatusBadRequest, "Login Failed")
        return
    }
    loginRes := LoginRes{
        Token:    token,
        UserName: tokenInfo.UserName,
        Name:     tokenInfo.Name,
        Role:     tokenInfo.Role,
    }
    bgin.GinSuccessResult(c, loginRes)
}

func IsLogin(c *gin.Context) {
    _, err := GetTokenInfo(c)
    if err != nil {
        bgin.GinSuccessResult(c, false)
    } else {
        bgin.GinSuccessResult(c, true)
    }
}

func RefreshToken(c *gin.Context) {
    tokenInfo, err := GetTokenInfo(c)
    if err != nil {
        bgin.GinFailResult(c, http.StatusForbidden, "Not Login")
        return
    }
    tokenInfo.Timestamp = time.Now().UnixMilli()
    token, err := CreateToken(tokenInfo)
    if err != nil {
        log.Errorln("login failed", err)
        bgin.GinFailResult(c, http.StatusBadRequest, "Refresh Error")
        return
    }
    loginRes := LoginRes{
        Token:    token,
        UserName: tokenInfo.UserName,
        Name:     tokenInfo.Name,
        Role:     tokenInfo.Role,
    }
    bgin.GinSuccessResult(c, loginRes)
}

func CreateToken(tokenInfo TokenInfo) (string, error) {
    tokenInfo.Timestamp = time.Now().UnixMilli()
    str, err := json.Marshal(tokenInfo)
    if err != nil {
        return "", err
    }

    token, err := aes.EnCode(str, []byte(tokenKey))
    if err != nil {
        return "", err
    }
    return string(token), nil
}

func ParseToken(token string) (TokenInfo, error) {
    data, err := aes.DeCode(token, []byte(tokenKey))
    if err != nil {
        return TokenInfo{}, err
    }
    tokenInfo := TokenInfo{}
    err = json.Unmarshal(data, &tokenInfo)
    if time.Now().UnixMilli()-tokenInfo.Timestamp > int64(TokenExpire) {
        return TokenInfo{}, fmt.Errorf("Login Error")
    }
    return tokenInfo, err
}

func GetTokenInfo(c *gin.Context) (TokenInfo, error) {
    token := c.GetHeader("token")
    if len(token) <= 0 {
        return TokenInfo{}, fmt.Errorf("token is empty")
    }
    tokenInfo, err := ParseToken(token)
    if err != nil {
        return TokenInfo{}, err
    }
    return tokenInfo, nil
}

func GetUserId(c *gin.Context) string {
    userId := c.GetHeader("userId")
    if len(userId) > 0 {
        return userId
    }
    tokenInfo, err := GetTokenInfo(c)
    if err != nil {
        return ""
    }
    return tokenInfo.UserId
}

func GetIntUserId(c *gin.Context) (uint64, error) {
    userIdStr := GetUserId(c)
    if len(userIdStr) == 0 {
        return 0, fmt.Errorf("user id is empty")
    }
    userId, err := strconv.ParseUint(userIdStr, 10, 64)
    if err != nil {
        log.Debug("", err)
    }
    return userId, err
}

//指定的url不进行登录验证
//ignoreUrls包含指定的Method [["GET","URL"],["PUT","URL"]]
//如果 ignoreUrls 的格式 [["URL"],["URL"]] 即未指定Method 则所有Method都忽略
func LoginAuthOut(ignoreUrls [][]string) gin.HandlerFunc {
    return func(c *gin.Context) {
        staticFile := str.InCall(bgin.Suffix, func(a string) bool {
            return strings.HasSuffix(c.Request.URL.Path, a)
        })
        if staticFile {
            c.Next()
            return
        }
        for _, u := range ignoreUrls {
            if len(u) == 1 && c.Request.URL.Path == u[0] {
                c.Next()
                return
            } else if u[0] == c.Request.Method && c.Request.URL.Path == u[1] {
                c.Next()
                return
            }
        }
        token := c.GetHeader("token")
        if len(token) == 0 {
            c.Abort()
            bgin.GinFailResult(c, http.StatusUnauthorized, "Not Login")
            return
        }
        tokenInfo, err := ParseToken(token)
        if err != nil {
            c.Abort()
            bgin.GinFailResult(c, http.StatusUnauthorized, "Not Login")
            return
        }
        c.Header("userId", tokenInfo.UserId)
        c.Next()
    }
}

//只有指定的url验证登录
//containUrls 包含指定的Method [["GET","URL"],["PUT","URL"]]
//如果 containUrls 的格式 [["URL"],["URL"]] 即未指定Method 则所有Method都忽略
func LoginAuthIn(containUrls [][]string) gin.HandlerFunc {
    return func(c *gin.Context) {
        isAuth := false
        for _, u := range containUrls {
            if len(u) == 1 && c.Request.URL.Path == u[0] {
                isAuth = true
                break
            } else if u[0] == c.Request.Method && c.Request.URL.Path == u[1] {
                isAuth = true
                break
            }
        }
        if !isAuth {
            c.Next()
            return
        }
        token := c.GetHeader("token")
        if len(token) == 0 {
            c.Abort()
            bgin.GinFailResult(c, http.StatusUnauthorized, "Not Login")
            return
        }
        tokenInfo, err := ParseToken(token)
        if err != nil {
            c.Abort()
            bgin.GinFailResult(c, http.StatusUnauthorized, "Not Login")
            return
        }
        c.Header("userId", tokenInfo.UserId)
        c.Next()
    }
}
