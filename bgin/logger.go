package bgin

import (
    "fmt"
    "io"
    "net/http"
    "time"

    "gitee.com/andy_pang/go-common/v2/log"

    "github.com/gin-gonic/gin"
)

var DefaultLogFormatter = func(param gin.LogFormatterParams) string {
    var statusColor, methodColor, resetColor string
    if param.IsOutputColor() {
        statusColor = param.StatusCodeColor()
        methodColor = param.MethodColor()
        resetColor = param.ResetColor()
    }

    if param.Latency > time.Minute {
        param.Latency = param.Latency.Truncate(time.Second)
    }
    return fmt.Sprintf("[GIN] %v |%s %3d %s| %13v | %15s |%s %-7s %s %#v\n%s",
        param.TimeStamp.Format("2006/01/02 - 15:04:05"),
        statusColor, param.StatusCode, resetColor,
        param.Latency,
        param.ClientIP,
        methodColor, param.Method, resetColor,
        param.Path,
        param.ErrorMessage,
    )
}

var LoggerHandler gin.HandlerFunc = DefaultLogHandler(log.LogWriter, 5*time.Second)

func DefaultLogHandler(out io.Writer, timeOut time.Duration) gin.HandlerFunc {
    return func(c *gin.Context) {
        // Start timer
        start := time.Now()
        path := c.Request.URL.Path
        raw := c.Request.URL.RawQuery

        // Process request
        c.Next()

        param := gin.LogFormatterParams{
            Request: c.Request,
            Keys:    c.Keys,
        }
        // Stop timer
        param.TimeStamp = time.Now()
        param.Latency = param.TimeStamp.Sub(start)
        param.ClientIP = c.ClientIP()
        param.Method = c.Request.Method
        param.StatusCode = c.Writer.Status()
        if param.Latency < timeOut && param.StatusCode == http.StatusOK {
            return
        }
        param.ErrorMessage = c.Errors.ByType(gin.ErrorTypePrivate).String()
        param.BodySize = c.Writer.Size()
        if raw != "" {
            path = path + "?" + raw
        }
        param.Path = path
        fmt.Fprint(out, DefaultLogFormatter(param))
    }
}
