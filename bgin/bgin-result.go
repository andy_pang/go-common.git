package bgin

import (
    "fmt"
    "net/http"

    "gitee.com/andy_pang/go-common/v2/common"
    "gitee.com/andy_pang/go-common/v2/log"
    "github.com/gin-gonic/gin"
)

func GinRsp(c *gin.Context, code int, obj any) {
    c.JSON(code, obj)
}

func GinResult(c *gin.Context, code int, msg string, data any) {
    rsp := common.Result{}
    rsp.Code = code
    rsp.Msg = msg
    rsp.Data = data
    GinRsp(c, code, rsp)
}

func GinSuccessResult(c *gin.Context, data any) {
    GinResult(c, http.StatusOK, "", data)
}

func GinFailResult(c *gin.Context, code int, msg string) {
    GinResult(c, code, msg, nil)
}

func GinErrorResult(c *gin.Context, err error) {
    log.Errorln(err)
    GinResult(c, http.StatusInternalServerError, fmt.Sprintf("%v", err), nil)
}

func GinBoolResult(c *gin.Context, err error) {
    if err != nil {
        log.Errorln(err)
        GinResult(c, http.StatusInternalServerError, fmt.Sprintf("%v", err), nil)
    } else {
        GinSuccessResult(c, true)
    }
}
