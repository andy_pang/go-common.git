//可以静态资源初始化
//js css 等文件从embed资源中读取
//其他路径全部读取首页内容 此方案适用于 vue react打包部署

package bgin

import (
    "embed"
    "fmt"
    "html/template"
    "net/http"
    "strings"

    "gitee.com/andy_pang/go-common/v2/common"
    "gitee.com/andy_pang/go-common/v2/config"
    "gitee.com/andy_pang/go-common/v2/log"
    "gitee.com/andy_pang/go-common/v2/utils/str"
    "github.com/gin-gonic/gin"
)

var Suffix = []string{".js", ".png", ".ico", ".js.map", ".css.map", ".css", ".json", ".svg"}
var dirName = "html"
var indexName = "index.html"

var RecoveryHandler gin.HandlerFunc = gin.Recovery()

func SetSuffix(s []string) {
    Suffix = s
}

func SetDirName(name string) {
    dirName = name
}

func SetIndexName(name string) {
    indexName = name
}

func StartHttp(fs *embed.FS, route func(*gin.Engine)) {
    gin.DefaultWriter = log.LogWriter
    gin.DefaultErrorWriter = log.LogWriter
    r := gin.New()
    r.Use(LoggerHandler)
    r.Use(RecoveryHandler)
    r.Use(cors())
    if fs != nil {
        InitStatic(r, *fs)
    }
    route(r)
    runAddr := fmt.Sprintf("0.0.0.0:%v", config.Server().Port)
    log.Info("Start server >", runAddr)
    fmt.Println("Start server success!")
    if err := r.Run(runAddr); err != nil {
        log.Errorln("Start server error!", err.Error())
    }
}

func InitStatic(r *gin.Engine, fs embed.FS) {
    t, _ := template.ParseFS(fs, dirName+"/"+indexName)
    r.SetHTMLTemplate(t)

    r.NoRoute(func(c *gin.Context) {
        staticServer := NewStaticServer(fs)
        staticServer.ServeHTTP(c)
    })
}

type StaticServer struct {
    fileHandler http.Handler
    fs          embed.FS
}

func NewStaticServer(fs embed.FS) *StaticServer {
    return &StaticServer{
        fs:          fs,
        fileHandler: http.FileServer(http.FS(fs)),
    }
}

func (f *StaticServer) ServeHTTP(c *gin.Context) {
    staticFile := str.InCall(Suffix, func(a string) bool {
        return strings.HasSuffix(c.Request.URL.Path, a)
    })
    if !staticFile {
        c.HTML(http.StatusOK, indexName, gin.H{})
        return
    } else {
        c.Request.URL.Path = dirName + c.Request.URL.Path
        f.fileHandler.ServeHTTP(c.Writer, c.Request)
        return
    }
}

func cors() gin.HandlerFunc {
    return func(c *gin.Context) {
        method := c.Request.Method
        origin := c.Request.Header.Get("Origin")
        if len(origin) > 0 && len(config.Cors().AllowOrigin) > 0 {
            c.Header("Access-Control-Allow-Origin", config.Cors().AllowOrigin) // 可将将 * 替换为指定的域名
            c.Header("Access-Control-Allow-Methods", config.Cors().AllowMethods)
            c.Header("Access-Control-Allow-Headers", config.Cors().AllowHeaders)
            c.Header("Access-Control-Expose-Headers", config.Cors().ExposeHeaders)
            c.Header("Access-Control-Allow-Credentials", config.Cors().AllowCredentials)
        }
        if method == "OPTIONS" {
            c.AbortWithStatus(http.StatusNoContent)
        }
        c.Next()
    }
}

//error 已在内部统一处理 返回只是作为标记 无特殊情况可不用处理
func ShouldBind(c *gin.Context, obj any) error {
    err := c.ShouldBind(obj)
    if err != nil {
        log.Debugln(err)
        GinFailResult(c, http.StatusBadRequest, "传参错误")
    }
    common.InitPageQuery(obj)
    return err
}
