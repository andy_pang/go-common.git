package db

import (
    "fmt"
    "io"
    "log"
    "path/filepath"
    "strings"
    "time"

    "gitee.com/andy_pang/go-common/v2/config"
    blog "gitee.com/andy_pang/go-common/v2/log"
    "gitee.com/andy_pang/go-common/v2/utils/bos"
    rotatelogs "github.com/lestrrat-go/file-rotatelogs"

    "gorm.io/driver/mysql"
    "gorm.io/driver/sqlite"
    "gorm.io/gorm"
    "gorm.io/gorm/logger"
)

const (
    DB_TYPE_MYSQL  = "mysql"
    DB_TYPE_SQLITE = "sqlite"
)

var db *gorm.DB

var dbLogger logger.Interface

func Init() (err error) {
    db, err = ConnectDb(config.Db())
    return
}

func ConnectDb(dbInfo config.DbInfo) (*gorm.DB, error) {
    var err error
    var dbUrl string
    var _db *gorm.DB
    if len(dbInfo.DbType) == 0 {
        return nil, fmt.Errorf("there is no db type, the db is not init")
    } else if dbInfo.DbType == DB_TYPE_MYSQL {
        dbUrl = buildConnUrl(dbInfo)
        _db, err = gorm.Open(mysql.Open(dbUrl), &gorm.Config{
            Logger: getDbLogger(),
        })
    } else if dbInfo.DbType == DB_TYPE_SQLITE {
        _db, err = gorm.Open(sqlite.Open(dbInfo.DbPath), &gorm.Config{
            Logger: getDbLogger(),
        })
    } else {
        return nil, fmt.Errorf("the db type %v is not supported", dbInfo.DbType)
    }

    if err != nil {
        blog.Debugln("db connection url " + dbUrl)
        return nil, err
    }
    poolSize := dbInfo.PoolSize
    if poolSize <= 0 {
        poolSize = 10
    }
    sqlDB, err := _db.DB()
    if err == nil {
        //设置数据库连接池参数
        sqlDB.SetMaxOpenConns(poolSize)
        sqlDB.SetMaxIdleConns(poolSize / 2)
    }
    return _db, nil
}

func buildConnUrl(conf config.DbInfo) string {
    cp := strings.Trim(conf.ConnParam, " ")
    if len(cp) == 0 {
        cp = "charset=utf8mb4&parseTime=true&loc=Asia%2FShanghai"
    }
    url := fmt.Sprintf("%s:%s@(%s)/%s?%s", conf.UserName, conf.Password, conf.Host, conf.DbName, cp)
    return url
}

func Stop() {

}

func Db() *gorm.DB {
    return db
}

//发生异常事回滚，异常仍然抛出去
func RollbackWhenPanic(tx *gorm.DB) {
    if e := recover(); e != nil {
        tx.Rollback()
        panic(e)
    }
}

func getDbLogger() logger.Interface {
    if dbLogger != nil {
        return dbLogger
    }
    var writer io.Writer
    path := getLogPath()
    if len(path) == 0 {
        writer = blog.LogWriter
    } else {
        writer = getLogWriter(path)
    }
    return logger.New(log.New(writer, "\r\n", log.LstdFlags), logger.Config{
        SlowThreshold:             200 * time.Millisecond,
        LogLevel:                  getLogLevel(),
        IgnoreRecordNotFoundError: false,
        Colorful:                  true,
    })
}

func getLogLevel() logger.LogLevel {
    level := strings.ToUpper(strings.Trim(config.Log().DbLevel, " "))
    if len(level) == 0 {
        return logger.Error
    }
    if level == "ERROR" {
        return logger.Error
    }
    if level == "WARN" {
        return logger.Warn
    }
    if level == "INFO" {
        return logger.Info
    }
    return logger.Error
}

func getLogWriter(path string) io.Writer {
    // 下面配置日志每隔 1 天轮转一个新文件，保留最近 30天的日志文件，多余的自动清理掉。
    dbLogWriter, _ := rotatelogs.New(
        path+".%Y%m%d",
        rotatelogs.WithLinkName(path),
        rotatelogs.WithMaxAge(time.Duration(24*30)*time.Hour),
        rotatelogs.WithRotationTime(time.Duration(24)*time.Hour),
    )
    return dbLogWriter
}

func getLogPath() string {
    logFileName := strings.Trim(config.Log().DbFileName, " ")
    if len(logFileName) == 0 {
        return ""
    }
    var path string
    if len(strings.Trim(config.Log().LogDir, " ")) == 0 {
        path = filepath.Join(bos.GetHomeDir(), "logs", logFileName)
    } else {
        path = filepath.Join(config.Log().LogDir, logFileName)
    }
    return path
}
